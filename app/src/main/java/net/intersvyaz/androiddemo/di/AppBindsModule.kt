package net.intersvyaz.androiddemo.di

import dagger.Binds
import dagger.Module
import net.intersvyaz.androiddemo.data.repository.SampleRepository
import net.intersvyaz.androiddemo.data.repository.SampleRepositoryImpl
import net.intersvyaz.androiddemo.domain.GetColorsUseCase
import net.intersvyaz.androiddemo.domain.GetColorsUseCaseImpl
import net.intersvyaz.androiddemo.domain.GetCurrentBrightnessUseCase
import net.intersvyaz.androiddemo.domain.GetCurrentBrightnessUseCaseImpl
import net.intersvyaz.androiddemo.domain.GetCurrentColorUseCase
import net.intersvyaz.androiddemo.domain.GetCurrentColorUseCaseImpl
import net.intersvyaz.androiddemo.domain.GetStateUseCase
import net.intersvyaz.androiddemo.domain.GetStateUseCaseImpl
import net.intersvyaz.androiddemo.domain.SetBrightnessUseCase
import net.intersvyaz.androiddemo.domain.SetBrightnessUseCaseImpl
import net.intersvyaz.androiddemo.domain.SetColorUseCase
import net.intersvyaz.androiddemo.domain.SetColorUseCaseImpl
import net.intersvyaz.androiddemo.domain.TurnOffUseCase
import net.intersvyaz.androiddemo.domain.TurnOffUseCaseImpl

import net.intersvyaz.androiddemo.domain.TurnOnUseCase
import net.intersvyaz.androiddemo.domain.TurnOnUseCaseImpl

@Module
interface AppBindsModule {
    @Binds
    fun bindSampleRepository(repository: SampleRepositoryImpl): SampleRepository

    @Binds
    fun bindGetColorsUseCase(useCase: GetColorsUseCaseImpl): GetColorsUseCase

    @Binds
    fun bindTurnOnUseCase(useCase: TurnOnUseCaseImpl): TurnOnUseCase

    @Binds
    fun bindTurnOffUseCase(useCase: TurnOffUseCaseImpl): TurnOffUseCase

    @Binds
    fun bindGetStateUseCase(useCase: GetStateUseCaseImpl): GetStateUseCase

    @Binds
    fun bindGetCurrentColorUseCase(useCase: GetCurrentColorUseCaseImpl): GetCurrentColorUseCase

    @Binds
    fun bindSetColorUseCase(useCase: SetColorUseCaseImpl): SetColorUseCase

    @Binds
    fun bindSetBrightnessUseCase(useCase: SetBrightnessUseCaseImpl): SetBrightnessUseCase

    @Binds
    fun bindGetCurrentBrightnessUseCase(useCase: GetCurrentBrightnessUseCaseImpl): GetCurrentBrightnessUseCase
}
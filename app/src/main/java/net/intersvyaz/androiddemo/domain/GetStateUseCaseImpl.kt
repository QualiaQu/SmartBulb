package net.intersvyaz.androiddemo.domain

import net.intersvyaz.androiddemo.data.repository.SampleRepository
import javax.inject.Inject

class GetStateUseCaseImpl @Inject constructor(
    private val repository: SampleRepository,
): GetStateUseCase {
    override suspend fun invoke(): Result<Boolean?> =
        repository.getState()
}


package net.intersvyaz.androiddemo.domain

interface TurnOffUseCase {
    suspend operator fun invoke(): Result<Boolean?>
}
package net.intersvyaz.androiddemo.domain

import net.intersvyaz.androiddemo.data.repository.SampleRepository
import javax.inject.Inject

class TurnOffUseCaseImpl @Inject constructor(
    private val repository: SampleRepository,
): TurnOffUseCase {
    override suspend fun invoke(): Result<Boolean?> =
        repository.turnOff()
}


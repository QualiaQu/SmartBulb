package net.intersvyaz.androiddemo.domain

interface GetStateUseCase {
    suspend operator fun invoke(): Result<Boolean?>
}


package net.intersvyaz.androiddemo.domain

import net.intersvyaz.androiddemo.data.repository.SampleRepository
import javax.inject.Inject

class GetCurrentBrightnessUseCaseImpl @Inject constructor(
    private val repository: SampleRepository,
): GetCurrentBrightnessUseCase {
    override suspend fun invoke(): Result<Int?> =
        repository.getCurrentBrightness()
}
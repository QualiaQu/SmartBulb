package net.intersvyaz.androiddemo.domain

import net.intersvyaz.androiddemo.data.repository.SampleRepository
import javax.inject.Inject

class TurnOnUseCaseImpl @Inject constructor(
    private val repository: SampleRepository,
): TurnOnUseCase {
    override suspend fun invoke(): Result<Boolean?> =
        repository.turnOn()
}


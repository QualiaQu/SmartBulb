package net.intersvyaz.androiddemo.domain

interface GetCurrentBrightnessUseCase {
    suspend operator fun invoke(): Result<Int?>
}
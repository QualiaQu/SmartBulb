package net.intersvyaz.androiddemo.domain

import net.intersvyaz.androiddemo.data.ColorResponse

interface GetCurrentColorUseCase {
    suspend operator fun invoke(): Result<ColorResponse?>
}
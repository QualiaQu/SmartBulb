package net.intersvyaz.androiddemo.domain

interface TurnOnUseCase {
    suspend operator fun invoke(): Result<Boolean?>
}


package net.intersvyaz.androiddemo.domain

import net.intersvyaz.androiddemo.data.ColorResponse
import net.intersvyaz.androiddemo.data.repository.SampleRepository
import javax.inject.Inject

class GetCurrentColorUseCaseImpl @Inject constructor(
    private val repository: SampleRepository,
): GetCurrentColorUseCase {
    override suspend fun invoke(): Result<ColorResponse?> =
        repository.getCurrentColor()
}
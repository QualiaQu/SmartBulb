package net.intersvyaz.androiddemo.data.repository

import net.intersvyaz.androiddemo.data.ColorResponse
import okhttp3.RequestBody
import retrofit2.Response

interface SampleRepository {
    suspend fun turnOn(): Result<Boolean?>
    suspend fun turnOff(): Result<Boolean?>
    suspend fun getState(): Result<Boolean?>
    suspend fun getColors(): Result<List<String>?>
    suspend fun getCurrentColor(): Result<ColorResponse?>
    suspend fun setColor(color: String): Result<Boolean?>
    suspend fun setBrightness(level: Int): Result<Boolean?>
    suspend fun getCurrentBrightness(): Result<Int?>

}


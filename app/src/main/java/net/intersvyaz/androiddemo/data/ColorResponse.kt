package net.intersvyaz.androiddemo.data

import net.intersvyaz.androiddemo.UiState


data class ColorResponse(
    val id: Int,
    val name: String,
    val type: String,
    val color: String
)

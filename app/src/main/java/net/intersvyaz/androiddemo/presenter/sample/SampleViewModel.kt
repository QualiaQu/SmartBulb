package net.intersvyaz.androiddemo.presenter.sample

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import net.intersvyaz.androiddemo.UiState
import net.intersvyaz.androiddemo.data.ColorResponse
import net.intersvyaz.androiddemo.domain.GetColorsUseCase
import net.intersvyaz.androiddemo.domain.GetCurrentBrightnessUseCase
import net.intersvyaz.androiddemo.domain.GetCurrentColorUseCase
import net.intersvyaz.androiddemo.domain.GetStateUseCase
import net.intersvyaz.androiddemo.domain.SetBrightnessUseCase
import net.intersvyaz.androiddemo.domain.SetColorUseCase
import net.intersvyaz.androiddemo.domain.TurnOffUseCase
import net.intersvyaz.androiddemo.domain.TurnOnUseCase
import net.intersvyaz.androiddemo.toUiState
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import javax.inject.Inject

class SampleViewModel @Inject constructor(
    private val getColorsUseCase: GetColorsUseCase,
    private val turnOnUseCase: TurnOnUseCase,
    private val turnOffUseCase: TurnOffUseCase,
    private val getStateUseCase: GetStateUseCase,
    private val getCurrentColorUseCase: GetCurrentColorUseCase,
    private val setColorUseCase: SetColorUseCase,
    private val setBrightnessUseCase: SetBrightnessUseCase,
    private val getBrightnessUseCase: GetCurrentBrightnessUseCase
): ViewModel() {


    private val _colors = MutableLiveData<UiState<List<String>?>>(UiState.Loading)
    private val _state = MutableLiveData<UiState<Boolean?>>(UiState.Loading)
    private val _currentColor = MutableLiveData<UiState<ColorResponse?>?>(UiState.Loading)
    private val _currentBrightness = MutableLiveData<UiState<Int?>?>(UiState.Loading)

    val colors: LiveData<UiState<List<String>?>>
        get() = _colors
    val state: LiveData<UiState<Boolean?>>
        get() = _state
    val currentColor: LiveData<UiState<ColorResponse?>?>
        get() = _currentColor
    val currentBrightness: LiveData<UiState<Int?>?>
        get() = _currentBrightness

    fun loadColors() {
        viewModelScope.launch {
            _colors.postValue(getColorsUseCase().toUiState())
        }
    }

    fun setColor(color: String) {
        viewModelScope.launch {
            setColorUseCase(color)
        }
    }

    fun setBrightness(level: Int) {
        viewModelScope.launch {
            setBrightnessUseCase(level)
        }
    }

    fun getBrightness(){
        viewModelScope.launch {
            _currentBrightness.postValue(getBrightnessUseCase().toUiState())
        }
    }

    fun turnOn(){
        viewModelScope.launch {
            turnOnUseCase()
        }
    }
    fun turnOff(){
        viewModelScope.launch {
            turnOffUseCase()
        }
    }

    fun getState(){
        viewModelScope.launch {
            _state.postValue(getStateUseCase().toUiState())
        }
    }

    fun getCurrentColor() {
        viewModelScope.launch {
            _currentColor.postValue(getCurrentColorUseCase().toUiState())
        }
    }
}


